const prettierrc = require('./.prettierrc')

module.exports = {
  env: {
    node: true,
    es6: true
  },
  parser: 'babel-eslint',
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module'
  },
  extends: ['standard', 'prettier', 'prettier/standard'],
  plugins: ['prettier'],
  rules: {
    complexity: ['error', 14],
    'vars-on-top': 'error',
    'require-await': 'error',
    'prettier/prettier': ['error', prettierrc],
    'no-unused-vars': [
      2,
      {
        argsIgnorePattern: '^_',
        varsIgnorePattern: '^ignored',
        args: 'after-used',
        ignoreRestSiblings: true
      }
    ],
    'no-console': 'warn',
    'no-debugger': 'error',
    'no-nested-ternary': 'warn',
    'no-process-exit': 'error',
    'no-process-env': 'off',
    'no-path-concat': 'error',
    'no-new': 'error',
    'no-new-wrappers': 'error',
    'no-new-func': 'error',
    'no-mixed-requires': ['error', { grouping: true, allowCall: false }],
    'no-else-return': 'off',
    'no-case-declarations': 'error',
    'guard-for-in': 'error',
    'dot-notation': 'error',
    'default-case': 'error'
  }
}
