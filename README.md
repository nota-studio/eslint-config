# eslint-config

Not a Studio's ESLint shared config.

## Usage

Install the package by running:

```sh
yarn add -D eslint @notastudio/eslint-config
```

Then add the extends to your `.eslintrc`:

```
{
  "extends": ["@notastudio"],
  "rules": {
    // your overrides
  }
}
```

Finally, for Prettier's rules to work with your editor / git hook add at `prettier.config.js`:

```js
module.exports = require('@notastudio/eslint-config/.prettierrc')
```

## Other configs

This config exposes a few other configs that you can use standalone or in combination with the base config:

```
{
  "extends": ["@notastudio", "@notastudio/eslint-config/<config-name>"]
}
```

- `react`: [React](https://www.npmjs.com/package/react) JS library (_eslint-plugin-react_)
- `jest`: [jest](http://facebook.github.io/jest/) testing framework (_eslint-plugin-jest_)
- `flow`: [flow](https://flow.org/) static type checker for JavaScript (_eslint-plugin-flowtype_)

## License

MIT
