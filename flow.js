module.exports = {
  plugins: ['flowtype'],
  extends: ['plugin:flowtype/recommended', 'prettier/flowtype'],
  settings: {
    flowtype: {
      onlyFilesWithFlowAnnotation: true
    }
  }
}
